package com.example.hcfb.myapplication2.model.data.local;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

public abstract class BaseProduct {

    @NonNull
    private String name;
    private int contractNumber;
    @NonNull
    private Date openedDate;
    @Nullable
    private String status;

    public BaseProduct(@NonNull String name, int contractNumber, @NonNull Date openedDate) {
        this.name = name;
        this.contractNumber = contractNumber;
        this.openedDate = openedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseProduct product = (BaseProduct) o;

        if (contractNumber != product.contractNumber) return false;
        if (!name.equals(product.name)) return false;
        if (!openedDate.equals(product.openedDate)) return false;
        return status != null ? status.equals(product.status) : product.status == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + contractNumber;
        result = 31 * result + openedDate.hashCode();
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public int getContractNumber() {
        return contractNumber;
    }

    public Date getOpenedDate() {
        return openedDate;
    }

    @NonNull
    public String getStatus() {
        return status == null ? "" : status;
    }
}
