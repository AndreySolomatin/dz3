package com.example.hcfb.myapplication2.model.data.local;

import android.support.annotation.NonNull;

import java.util.Date;

public class DepositProduct extends BaseProduct {

    private double amount;

    public DepositProduct(@NonNull String name, int contractNumber, @NonNull Date openedDate, double amount) {
        super(name, contractNumber, openedDate);
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
