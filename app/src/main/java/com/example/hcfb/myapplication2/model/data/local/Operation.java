package com.example.hcfb.myapplication2.model.data.local;

import android.support.annotation.NonNull;

public class Operation {

    private int contractNumber;
    private long id;
    private double amount;

    public Operation(int contractNumber, long id, double amount) {
        this.contractNumber = contractNumber;
        this.id = id;
        this.amount = amount;
    }

    public int getContractNumber() {
        return contractNumber;
    }

    public long getId() {
        return id;
    }

    @NonNull
    public double getAmount() {
        return amount;
    }
}
