package com.example.hcfb.myapplication2.model.data.local;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ProductStatus {
}
