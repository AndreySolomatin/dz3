package com.example.hcfb.myapplication2.model.repository;

import android.support.annotation.NonNull;

import com.example.hcfb.myapplication2.model.data.local.Operation;

import java.util.List;

public interface OperationsRepository {

    @NonNull
    Operation getOperationById(long id);

    @NonNull
    List<Operation> getOperationByContractNumber(int contractNumber);

    void setOperations(@NonNull List<Operation> operations, int contractNumber);
}
