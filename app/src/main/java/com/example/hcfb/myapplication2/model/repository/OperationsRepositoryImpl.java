package com.example.hcfb.myapplication2.model.repository;

import android.support.annotation.NonNull;

import com.example.hcfb.myapplication2.model.data.local.Operation;

import java.util.HashMap;
import java.util.List;

public class OperationsRepositoryImpl implements OperationsRepository {

    @NonNull
    private HashMap<Long, Operation> operationsById;
    @NonNull
    private HashMap<Integer, List<Operation>> operationsByContractNumber;

    public OperationsRepositoryImpl() {
        this.operationsById = new HashMap<>();
        this.operationsByContractNumber = new HashMap<>();
    }

    @NonNull
    @Override
    public Operation getOperationById(long id) {
        return operationsById.get(id);
    }

    @NonNull
    @Override
    public List<Operation> getOperationByContractNumber(int contractNumber) {
        return operationsByContractNumber.get(contractNumber).sort();
        //dfgdfgdfgdfgdf
        //dfgdfgdfgdffdg dev
        //dfgdfgd fgdf gdfgdfgdfgdfg
    }

    @Override
    public void setOperations(@NonNull List<Operation> operations, int contractNumber) {
        operationsByContractNumber.put(contractNumber, operations);
        for (Operation operation : operations) {
            operationsById.put(operation.getId(), operation);
        }
    }
}
