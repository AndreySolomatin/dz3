package com.example.hcfb.myapplication2.ui.products;

import android.support.annotation.NonNull;

import com.example.hcfb.myapplication2.model.data.local.BaseProduct;
import com.example.hcfb.myapplication2.ui.products.items.BaseItem;

import java.util.List;

public interface ProductsItemBuilder {

    @NonNull
    List<BaseItem> build(@NonNull List<BaseProduct> products);

}
