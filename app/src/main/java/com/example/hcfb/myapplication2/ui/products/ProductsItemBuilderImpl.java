package com.example.hcfb.myapplication2.ui.products;

import android.support.annotation.NonNull;

import com.example.hcfb.myapplication2.model.data.local.BaseProduct;
import com.example.hcfb.myapplication2.model.data.local.DepositProduct;
import com.example.hcfb.myapplication2.ui.products.items.BaseItem;
import com.example.hcfb.myapplication2.ui.products.items.DepositItem;
import com.example.hcfb.myapplication2.ui.products.items.TitleItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

class ProductsItemBuilderImpl implements ProductsItemBuilder {

    @NonNull
    @Override
    public List<BaseItem> build(@NonNull List<BaseProduct> products) {
        List<BaseItem> items = new ArrayList<>();
        buildDeposits(products, items);
        return items;
    }

    private void buildDeposits(@NonNull List<BaseProduct> products,
                                 @NonNull List<BaseItem> items) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        boolean isTitleAdded = false;
        for (BaseProduct product : products) {
            if (product instanceof DepositProduct) {
                if (!product.getStatus().equals("closed")) {
                    if (!isTitleAdded) {
                        items.add(new TitleItem("Вклады"));
                        isTitleAdded = true;
                    }
                    items.add(new DepositItem(product.getName(),
                            ((DepositProduct) product).getAmount(),
                            simpleDateFormat.format(product.getOpenedDate().toString())));
                }
            }
        }
    }

}
