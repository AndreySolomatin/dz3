package com.example.hcfb.myapplication2.ui.products.items;

import android.support.annotation.NonNull;

public class DepositItem extends BaseItem {

    @NonNull
    private String name;
    private double amount;
    private String openedDate;

    public DepositItem(@NonNull String name, double amount, String openedDate) {
        this.name = name;
        this.amount = amount;
        this.openedDate = openedDate;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }

    public String getOpenedDate() {
        return openedDate;
    }
}
