package com.example.hcfb.myapplication2.ui.products.items;

import android.support.annotation.NonNull;

public class TitleItem extends BaseItem {

    @NonNull
    private String title;

    public TitleItem(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getTitle() {
        return title;
    }
}
